<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

// Entities
use App\Entity\Card; use App\Form\CardType;
use App\Entity\Faction; use App\Form\FactionType;
use App\Entity\Deck; use App\Form\DeckType;
use App\Entity\DeckCard;

/**
 * @Route("/card")
 */
class CardController extends AbstractController
{
    /**
     * @Route("/", name="card")
     */
    public function card(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $card = new Card();
        $formCard = $this->createForm(CardType::class, $card);

        $cards = $em->getRepository(Card::class)->findAll();

        $formCard->handleRequest($request);
        if ($formCard->isSubmitted() && $formCard->isValid()) {

            $card->setCreator($this->getUser());

            $image = $formCard->get('image')->getData();

            $imageName = 'card-'.uniqid().'.'.$image->guessExtension();

            $image->move(
                $this->getParameter('cards_folder'),
                $imageName
            );

            $card->setImage($imageName);
           
            $em->persist($card);
            $em->flush();
    
            return new Response($card->getName().' created');
        }

        return $this->render('card/index.html.twig', [
            'entities' => $cards,
            'form' => $formCard->createView()
        ]);
    }

    /**
     * @Route("/faction", name="faction")
     */
    public function faction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $faction = new Faction();
        $formFaction = $this->createForm(FactionType::class, $faction);

        $factions = $em->getRepository(Faction::class)->findAll();

        $formFaction->handleRequest($request);
        if ($formFaction->isSubmitted() && $formFaction->isValid()) {
           
            $em->persist($faction);
            $em->flush();
    
            return new Response($faction->getName().' created');
        }

        return $this->render('card/index.html.twig', [
            'entities' => $factions,
            'form' => $formFaction->createView()
        ]);
    }

    /**
     * @Route("/deck", name="deck")
     */
    public function deck(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $deck = new Deck();
        $formDeck = $this->createForm(DeckType::class, $deck);

        $decks = $em->getRepository(Deck::class)->findAll();

        $formDeck->handleRequest($request);
        if ($formDeck->isSubmitted() && $formDeck->isValid()) {

            $deck->setUser($this->getUser());
           
            $em->persist($deck);
            $em->flush();
    
            return new Response('Deck '.$deck->getName().' created');
        }

        return $this->render('card/index.html.twig', [
            'entities' => $decks,
            'form' => $formDeck->createView()
        ]);
    }

    /**
     * @Route("/cards-list", name="cardsList")
     */
    public function cardsList()
    {
        $em = $this->getDoctrine()->getManager();

        $cards = $em->getRepository(Card::class)->findAll();

        return $this->render('card/cardsList.html.twig', [
            'cards' => $cards,
        ]);
    }

    /**
     * @Route("/decks-list", name="decksList")
     */
    public function decksList()
    {
        $em = $this->getDoctrine()->getManager();

        $decks = $em->getRepository(Deck::class)->findByUser($this->getUser());

        return $this->render('card/decksList.html.twig', [
            'decks' => $decks,
        ]);
    }

    /**
     * @Route("/deck-manage/{idDeck}", name="deckManage")
     */
    public function deckManage(int $idDeck)
    {
        $em = $this->getDoctrine()->getManager();

        $deck = $em->getRepository(Deck::class)->findOneBy([
            'user' => $this->getUser(),
            'id' => $idDeck
        ]);

        if(is_null($deck)) {
            return new RedirectResponse($this->urlGenerator->generate('index'));
        }

        $cards = $em->getRepository(Card::class)->findAll();

        return $this->render('card/deckManage.html.twig', [
            'deck' => $deck,
            'cards' => $cards
        ]);
    }

    /**
     * @Route("/deck-add/{idDeck}", name="deckAdd")
     */
    public function deckAdd(int $idDeck, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $deck = $em->getRepository(Deck::class)->findOneBy([
            'user' => $this->getUser(),
            'id' => $idDeck
        ]);

        if(is_null($deck)) {
            return new RedirectResponse($this->urlGenerator->generate('index'));
        }

        $card = $em->getRepository(Card::class)->findOneById($request->request->get('idCard'));

        $deckCard = new DeckCard();

        $deckCard->setDeck($deck);
        $deckCard->setCard($card);
        $em->persist($deckCard);
        $em->flush();

        return new Response($card->getName().' added to deck '.$deck->getName());
    }

    /**
     * @Route("/deck-remove/{idDeck}", name="deckRemove")
     */
    public function deckRemove(int $idDeck, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $deck = $em->getRepository(Deck::class)->findOneBy([
            'user' => $this->getUser(),
            'id' => $idDeck
        ]);

        if(is_null($deck)) {
            return new RedirectResponse($this->urlGenerator->generate('index'));
        }

        $deckCard = $em->getRepository(DeckCard::class)->findOneBy([
            'card' => $request->request->get('idCard'),
            'deck' => $deck
        ]);

        $em->remove($deckCard);
        $em->flush();

        return new Response('Card removed from deck '.$deck->getName());
    }

    /**
     * @Route("/card-export", name="cardExport")
     */
    public function cardExport(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $cards = $em->getRepository(Card::class)->findAll();

        $export = fopen('php://output', 'r+');
        foreach ($cards as $card) {
            fputcsv($export, $card->arrayExport());
        }
        fclose($export);
        $response = new Response();

        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition','attachment; filename="export.csv"');

        return $response;

    }
}
